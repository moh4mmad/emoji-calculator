<?php

namespace App\Calculator\Operators;

final class Multiplication
{
    public function run($input1, $input2): float
    {
        return  $input1 * $input2;
    }
}
