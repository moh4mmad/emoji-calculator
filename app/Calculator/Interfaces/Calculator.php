<?php

namespace App\Calculator\Interfaces;

interface Calculator
{
    public function calculation($request);
}
