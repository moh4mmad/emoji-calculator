<?php

namespace App\Calculator;

use App\Calculator\Interfaces\Calculator as CalculatorInterface;
use App\Calculator\Operators\Addition;
use App\Calculator\Operators\Subtraction;
use App\Calculator\Operators\Division;
use App\Calculator\Operators\Multiplication;

class Calculator implements CalculatorInterface
{

    public function __construct()
    {
        $this->operands = [
            "addition" => new Addition(),
            "subtraction" => new Subtraction(),
            "multiplication" => new Multiplication(),
            "division" => new Division()
        ];
    }

    public function calculation($request)
    {
        $class = $this->operands[$request->operator];
        return $class->run($request->first_value, $request->second_value);
    }
}
