<?php

namespace App\Http\Controllers;

use App\Calculator\Calculator;
use App\Http\Requests\CalculationRequest;
use Illuminate\Http\JsonResponse;
use Throwable;

class CalculatorController extends Controller
{

    public function calculator(CalculationRequest $request, Calculator $calculator): JsonResponse
    {
        try {
            $result = $calculator->calculation($request);
            return response()->json([
                'status' => 'success',
                'result' => $result
            ], 200);
        } catch (Throwable $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }
}
