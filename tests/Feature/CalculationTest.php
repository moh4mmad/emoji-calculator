<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    /**
     * Calculation test without operator , first_value & second_value
     *
     * @return void
     */

    public function test_WithoutoperatorFirstAndSecondValue()
    {
        $this->json('POST', 'api/calculator')
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    'operator' => ["The operator field is required."],
                    'first_value' => ["The first value field is required."],
                    'second_value' => ["The second value field is required."],
                ]
            ]);
    }

    /**
     * Addition Calculation test with operator , first_value & second_value
     *
     * @return void
     */

    public function test_CalculationAddition()
    {

        $data = ['operator' => 'addition', 'first_value' => 1, 'second_value' => 2];

        $this->json('POST', 'api/calculator', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "status" => 'success',
                "result" => 3
            ]);
    }

    /**
     * multiplication Calculation test with operator , first_value & second_value
     *
     * @return void
     */

    public function test_CalculationMultiplication()
    {

        $data = ['operator' => 'multiplication', 'first_value' => 10, 'second_value' => 5];

        $this->json('POST', 'api/calculator', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "status" => 'success',
                "result" => 50
            ]);
    }

    /**
     * Subtraction Calculation test with operator , first_value & second_value
     *
     * @return void
     */

    public function test_CalculationSubtraction()
    {

        $data = ['operator' => 'subtraction', 'first_value' => 10, 'second_value' => 5];

        $this->json('POST', 'api/calculator', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "status" => 'success',
                "result" => 5
            ]);
    }

    /**
     * Division Calculation test with operator , first_value & second_value
     *
     * @return void
     */

    public function test_CalculationDivision()
    {

        $data = ['operator' => 'division', 'first_value' => 10, 'second_value' => 5];

        $this->json('POST', 'api/calculator', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "status" => 'success',
                "result" => 2
            ]);
    }
}
