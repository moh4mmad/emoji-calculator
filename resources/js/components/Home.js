import React from "react";
import ReactDOM from "react-dom";
import "./Home.css";
import axios from "axios";
import Swal from "sweetalert2";

class Home extends React.Component {
    state = {
        operator: "",
        first_value: "",
        second_value: "",
        submission: false,
    };

    static get API() {
        return "http://127.0.0.1:8000/api/";
    }

    calculate = async (event) => {
        event.preventDefault();
        let inputError = this.inputCheck({
            Emoji: this.state.operator,
            "First Value": this.state.first_value,
            "Second Value": this.state.second_value,
        });
        if (!inputError) {
            this.state.submission = true;
            try {
                const response = await axios.post(
                    this.constructor.API + "calculator",
                    this.state
                );
                this.state.submission = false;
                Swal.fire({
                    title: `<strong>Result</strong>`,
                    html: `<b>` + response.data.result + `</b>`,
                    icon: "success",
                });
            } catch (error) {
                this.state.submission = false;
                if (error.response) {
                    let msg = "";
                    for (let field of Object.keys(error.response.data.errors)) {
                        msg += error.response.data.errors[field][0] + "<br>";
                    }
                    Swal.fire({
                        title: `<strong>Error</strong>`,
                        html: `<b>` + msg + `</b>`,
                        icon: "error",
                    });
                }
            }
        }
    };

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    };

    inputCheck = (inputs) => {
        let msg = "";
        let error = false;
        Object.entries(inputs).map(([key, value]) => {
            if (value === "") {
                error = true;
                msg += "The " + key + " is required.<br>";
            }
        });
        if (msg !== "") {
            Swal.fire({
                title: `<strong>Input error!</strong>`,
                html: `<b>` + msg + `</b>`,
                icon: "error",
            });
        }
        return error;
    };

    render() {
        return (
            <div>
                <div
                    className="min-vh-100 d-flex flex-column opacity-mask"
                    data-opacity-mask="rgba(0, 0, 0, 0.05)"
                >
                    <div className="container my-auto">
                        <div className="row">
                            <div className="col-md-9 col-lg-7 col-xl-6 mx-auto my-4">
                                <div className="panel center">
                                    <figure className="add_bottom_30">
                                        <h2>Emoji Based Calculator</h2>
                                    </figure>
                                    <form
                                        className="form_input"
                                        onSubmit={this.calculate}
                                    >
                                        <div className="operators mb-4">
                                            <ul className="clearfix">
                                                <li>
                                                    <div className="container_numbers">
                                                        <input
                                                            type="radio"
                                                            id="addition"
                                                            name="operator"
                                                            value="addition"
                                                            onChange={
                                                                this
                                                                    .handleChange
                                                            }
                                                        />
                                                        <label
                                                            className="radio addition"
                                                            title="Addition"
                                                            htmlFor="addition"
                                                        >
                                                            &#128125;
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="container_numbers">
                                                        <input
                                                            type="radio"
                                                            id="subtraction"
                                                            name="operator"
                                                            value="subtraction"
                                                            onChange={
                                                                this
                                                                    .handleChange
                                                            }
                                                        />
                                                        <label
                                                            className="radio subtraction"
                                                            title="Subtraction"
                                                            htmlFor="subtraction"
                                                        >
                                                            &#128128;
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="container_numbers">
                                                        <input
                                                            type="radio"
                                                            id="multiplication"
                                                            name="operator"
                                                            value="multiplication"
                                                            onChange={
                                                                this
                                                                    .handleChange
                                                            }
                                                        />
                                                        <label
                                                            className="radio multiplication"
                                                            title="Multiplication"
                                                            htmlFor="multiplication"
                                                        >
                                                            &#128123;
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="container_numbers">
                                                        <input
                                                            type="radio"
                                                            id="division"
                                                            name="operator"
                                                            value="division"
                                                            onChange={
                                                                this
                                                                    .handleChange
                                                            }
                                                        />
                                                        <label
                                                            className="radio division"
                                                            title="Division"
                                                            htmlFor="division"
                                                        >
                                                            &#128561;
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="first">
                                                First Value
                                            </label>
                                            <input
                                                type="text"
                                                name="first_value"
                                                id="first"
                                                className="form-control"
                                                autoComplete="off"
                                                onChange={this.handleChange}
                                            />
                                        </div>
                                        <div className="form-group mb-4">
                                            <label htmlFor="second">
                                                Second Value
                                            </label>
                                            <input
                                                type="text"
                                                name="second_value"
                                                id="second"
                                                className="form-control"
                                                autoComplete="off"
                                                onChange={this.handleChange}
                                            />
                                        </div>

                                        <button
                                            disabled={this.state.submission}
                                            type="submit"
                                            className="btn_1 full-width"
                                        >
                                            Calculate
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;

if (document.getElementById("home")) {
    ReactDOM.render(<Home />, document.getElementById("home"));
}
