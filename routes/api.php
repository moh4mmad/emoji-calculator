<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CalculatorController;

Route::middleware('throttle:40,1')->post('calculator', [CalculatorController::class, 'calculator']);
