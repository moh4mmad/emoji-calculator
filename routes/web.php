<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;

Route::get('/{any}', [PagesController::class, 'index'])->where('any', '.*');
