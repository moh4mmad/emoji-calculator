# CALCULATOR BASED ON EMOJIS (Implemented with Laravel & ReactJs)

## Approach

-   First taking input from users ( Operator & two text fields for operands)
-   Calculations are being done based on user input. For example, if user select alien then this application will perform addition.
-   After the calculations is done on the server side, the result is responded via JSON over HTTP protocol.

## Installation

-   `git clone https://gitlab.com/moh4mmad/emoji-calculator.git`
-   `cd ./emoji-calculator`
-   `composer install`
-   `cp .env.example .env`
-   `php artisan key:generate`
-   `php artisan serve`
- The application should be served in 127.0.0.1:8000
